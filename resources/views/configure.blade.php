@extends('installer::layout')

{{-- PAGE TITLE --}}
@section('title')
    Synergy Installer
@stop

{{-- PAGE CONTENT --}}
@section('page')

    <div class="jumbotron">
        <h1>Synergy Installer</h1>
        <p>
            The web installer is currently unavailable. Please run the installer from your command-line.
        </p>
    </div>

@stop
