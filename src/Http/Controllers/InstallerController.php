<?php

namespace Synergy\Installer\Http\Controllers;

use Illuminate\Routing\Controller;

/**
 * Part of the Installer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Installer
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://gitlab.com/
 */

 class InstallerController extends Controller
 {
     public function index()
     {
         return view('installer::configure');
     }
 }
