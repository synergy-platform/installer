<?php

namespace Synergy\Installer\Console\Commands;

use Illuminate\Console\Command;
use Synergy\Installer\Installer;
use Synergy\Modules\Modules;
use Synergy\Themes\Themes;

/**
 * Part of the Installer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Installer
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://gitlab.com/synergy-platform/installer
 */


class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synergy:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs Synergy';

    /**
     * Instance of the Synergy Installer
     *
     * @var \Synergy\Installer\Installer
     */
	protected $installer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Installer $installer, Modules $modules)
    {
        parent::__construct();

		$this->installer = $installer;

        $this->modules = $modules;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->installer->install();

        $this->info('Synergy has been installed!');

    }
}
